# Algorithmic Fantasy in A Flat

An ambient composition for SuperCollider, written in February 2023, album published 2023-03-02.

These files are the sourcecode to my composition. I do my best to check if the code is working as intended before publication; however, I cannot give any guarantees that the information they contain is complete and accurate, nor can I guarantee that they will produce the intended results on your machine.

## Dependencies
 * [Mi Ugens](https://github.com/v7b1/mi-UGens)
 * [Scale Conversion](https://framagit.org/emergent/ScaleConversion)

## License & Recording
Code licensed under the [Peer Production License](https://civicwise.org/peer-production-licence-_-human-readable-summary/).  
Recording available on [BandCamp](https://emergent.bandcamp.com/track/algorithmic-fantasy-in-a-flat). 
