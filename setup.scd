// a general-purpose setup file
(
s.options.memSize_(2.pow(16));
//s.options.inDevice_("ASIO : ASIO Digidesign Driver Mbox2"); // adjust this to the input/output devices of your choice
//s.options.outDevice_("ASIO : ASIO Digidesign Driver Mbox2"); // adjust this to the input/output devices of your choice
s.options.inDevice_(nil);
s.options.outDevice_(nil);
s.options.sampleRate_("44100");
s.options.recHeaderFormat_("wav");
s.options.recSampleFormat_("int24");

~out = 0;
t = TempoClock(60/60).permanent_(true);

~makeBuses = {
	~bus = Dictionary.new;
	~bus.add(\reverb -> Bus.audio(s, 2));
	~bus.add(\delay -> Bus.audio(s, 2));
	~bus.add(\endstage -> Bus.audio(s, 2));
};

~makeNodes = {
	~src = Group.new;
	~efx = Group.after(~src);
	~estg = Group.after(~efx);

	~revSynth = Synth(\reverb, [
		\in, ~bus[\reverb],
		\out, ~bus[\endstage]
	], ~efx, \addToTail);

	~delSynth = Synth(\delay, [
		\in, ~bus[\delay],
		\dry, 0.5,
		\dtime, t.beatDur * 0.75,
		\decay, t.beatDur * 2.5, 
		\out, ~bus[\endstage]
	], ~efx, \addToHead);

	~compSynth = Synth(\comp, [
		\in, ~bus[\endstage],
		\out, ~out
	], ~estg)
};

~cleanup = {
	"see you!".postln;
	Window.closeAll;
	s.newBusAllocators;
	ServerBoot.removeAll;
	ServerTree.removeAll;
	ServerQuit.removeAll;
};

//////////// register functions
ServerBoot.add(~makeBuses);
ServerQuit.add(~cleanup);

s.waitForBoot({
	// s.meter.window.alwaysOnTop_(true);

	s.sync;

	SynthDef(\reverb, {
		var sig;
		sig = In.ar(\in.ir(0), 2);
		sig = MiVerb.ar(
			sig,
			\time.kr(0.9),
			\drywet.kr(0.75),
			\damp.kr(0.5),
			\hp.kr(0.05),
			diff: \diff.kr(0.625),
			mul: \revAmp.kr(0.dbamp)
		);

		Out.ar(\out.ir(0), sig);
	}).add;

	SynthDef(\delay, {
		arg dry;
		var sig;

		sig = In.ar(\in.ir(0), 2);

		sig = AllpassC.ar(
			sig,
			\maxdel.kr(2),
			\dtime.kr(0.2),
			\decay.kr(1), 
			\delAmp.kr(0.dbamp)
		);

		Out.ar(\out.ir(0), sig * dry);
		Out.ar(\fx.ir(0), sig * (1-dry));
	}).add;

	SynthDef(\comp, {
		var sig;
		sig = In.ar(\in.ir(0), 2);
		sig = Compander.ar(
			sig,
			sig,
			\thresh.kr(0.6),
			\slopeBelow.kr(1.0),
			\slopeAbove.kr(0.66),
			mul: \compAmp.kr(3.dbamp)
		);
		Out.ar(\out.ir(0), sig);
	}).add;

	s.sync;

	ServerTree.add(~makeNodes);

	s.sync;	
	s.freeAll;
	s.sync;

	"do eet".postln;
});
)
